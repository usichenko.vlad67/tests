import os


def write_to_file(filename: str, phrase):
    if not os.path.exists(filename):
        raise FileNotFoundError
    if not isinstance(phrase, str):
        raise TypeError('phrase arg may be a string')
    with open(filename, 'w') as file:
        file.write(phrase)
