import pytest
from unittest.mock import patch, mock_open


@pytest.fixture()
def mocked_open():
    with patch('builtins.open', mock_open(read_data="Mock open as fixture")) as mock:
        yield mock
