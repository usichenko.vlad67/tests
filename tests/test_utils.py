import pytest
from unittest.mock import mock_open, patch, call, MagicMock
from main import write_to_file
test_file_1 = 'tests/testfile.txt'
test_file_2 = 'tests/testfile_2.txt'


def test_write_to_file_path_exc():
    with pytest.raises(FileNotFoundError) as exc:
        write_to_file("not_real_file.txt", 'Boo')
    assert FileNotFoundError == exc.type


def test_write_to_phrase_type_exc():
    with pytest.raises(TypeError) as exc:
        write_to_file(test_file_1, ['not a string', ])
    assert TypeError == exc.type


@pytest.mark.parametrize('input_, expected_data', [
    ((test_file_1, 'Hello World'), 'Hello World'),
    ((test_file_2, "Bye World"), "Bye World")
])
def test_write_to_file(input_, expected_data):

    with patch("builtins.open", mock_open(read_data=input_[1])) as open_mock:
        write_to_file(*input_)
        open_mock.assert_called_once_with(input_[0], 'w')
        open_mock().write.assert_called_once_with(input_[1])
        with open(input_[0], 'r') as file:
            actual_data = file.read()
    print(f"{input_=} | {expected_data=} | {actual_data=}")
    assert actual_data == expected_data


def test_write_to_file_mock_fixture(mocked_open):
    phrase = 'Mock open as fixture'
    write_to_file(test_file_1, phrase)
    with open(test_file_1, 'r') as file:
        actual_data = file.read()
    expected_data = phrase
    assert expected_data == actual_data
